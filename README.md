## RF Mixer with integrated LO

RF mixer development board with integrated LO. The board is based on RFFC2071.


## Development

Use KiCAD 5.1 or later.

## Licence

License: ![CERN Open Hardware Licence v1.2](LICENSE)

[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202014--2020-Libre%20Space%20Foundation-6672D8.svg)](https://librespacefoundation.org/)